package myTaxi;

import org.testng.annotations.Test;

import pageObjects.LandingPageObjects;
import support.DriverBase;

import org.testng.AssertJUnit;
import java.io.IOException;
import org.openqa.selenium.WebDriver;

public class LandingPageActions extends DriverBase{
	public WebDriver driver;
	
	@Test(priority = 1)
	public void goToUrl() throws IOException {
		driver = initializerDriver();
		driver.get(prop.getProperty("url"));
	}
		
	@Test(priority = 2)
	public void selectShoes() throws InterruptedException {
		LandingPageObjects lp = new LandingPageObjects(driver);
		lp.firstSection().click();
	}
	
	@Test(priority = 3)
	public void selectFirstProduct() throws InterruptedException {
		LandingPageObjects lp = new LandingPageObjects(driver);
		lp.firstSelection().click();
	}
	
	@Test(priority = 4)
	public void selectSize() throws InterruptedException {
		LandingPageObjects lp = new LandingPageObjects(driver);
		lp.sizeSelectionButton().click();
		lp.selectSize().click();
	}
	
	@Test(priority = 5)
	public void addToCart() throws InterruptedException {
		LandingPageObjects lp = new LandingPageObjects(driver);
		lp.addToCart().click();
	}
	
	@Test(priority = 6)
	public void secondProduct() throws InterruptedException {
		LandingPageObjects lp = new LandingPageObjects(driver);
		lp.secondSection().click();	
	}
	
	@Test(priority = 7)
	public void selectSecondProduct() throws InterruptedException {
		LandingPageObjects lp = new LandingPageObjects(driver);
		lp.secondSelection().click();
		selectSize();
		addToCart();
	}
	
	@Test(priority = 8)
	public void verifyCartSize() throws InterruptedException {
		Thread.sleep(3000);
		LandingPageObjects lp = new LandingPageObjects(driver);
		String checkSize = lp.carSize();
		int actual = Integer.parseInt(checkSize);
		int expected = 2;
		AssertJUnit.assertEquals(expected, actual);
		System.out.println();
	}
	
	@Test(priority = 9)
	public void quit() throws InterruptedException {
		Thread.sleep(4000);
		driver.quit();
	}	
}
