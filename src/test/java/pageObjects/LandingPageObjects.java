package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import support.DriverBase;

public class LandingPageObjects extends DriverBase {
	
	public WebDriver driver;
	By first_section = By.cssSelector("#unisexShoesNav > a:nth-child(1) > span:nth-child(1)");
	By second_section = By.cssSelector("li:nth-child(3) > span > a > span:nth-child(1)");
	By first_selection = By.cssSelector("z-grid-item[class^='z-nvg-cognac_articleCard']:nth-child(4)");
	By second_selection = By.cssSelector("z-grid-item[class^='z-nvg-cognac_articleCard']:nth-child(3)");
	By select_size_button = By.cssSelector(".h-dropdown-placeholder");
	By select_size = By.cssSelector("div[id^='h-dropdown-']:nth-child(2)");
	By add_to_cart_button = By.id("z-pdp-topSection-addToCartButton");
	By cart_size = By.cssSelector("z-coast-fjord-miniCart_items");
	By test = By.cssSelector("span[data-reactid='140']");
	public LandingPageObjects(WebDriver driver) {
		this.driver = driver;
	}
	
	public By elementClickable(By element) {
		WebDriverWait wait = new WebDriverWait(driver, 3);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		
		return element;
	}
	
	public By scrollToElement(By element) {
		if (!((WebElement) element).isDisplayed()) {
			Actions actions = new Actions(driver);
			actions.moveToElement((WebElement) element);
			actions.perform();
		}
		return element;
	}
	
	public WebElement firstSection() throws InterruptedException {
		elementClickable(first_section);
		return driver.findElement(first_section);
	}
	public WebElement secondSection() throws InterruptedException {
		elementClickable(second_section);
		return driver.findElement(second_section);
	}
	
	public WebElement firstSelection() throws InterruptedException {
		WebElement element = driver.findElement(first_selection);
		
		if (!element.isDisplayed()) {
			Actions actions = new Actions(driver);
			actions.moveToElement(element);
			actions.perform();
		}
		return element;
	}
	
	public WebElement secondSelection() throws InterruptedException {
		WebElement element = driver.findElement(second_selection);
		if (!element.isDisplayed()) {
			Actions actions = new Actions(driver);
			actions.moveToElement(element);
			actions.perform();
		}
		
		return element;
	}
	
	public WebElement sizeSelectionButton() throws InterruptedException {
		elementClickable(select_size_button);
		return driver.findElement(select_size_button);
	}
	
	public WebElement selectSize() throws InterruptedException {
		WebElement element = driver.findElement(select_size);
		elementClickable(select_size);
		if (!element.isDisplayed()) {
			Actions actions = new Actions(driver);
			actions.moveToElement(element);
			actions.perform();
			elementClickable(select_size);
		}
		return element;
	}
	
	public WebElement addToCart() throws InterruptedException {
		elementClickable(add_to_cart_button);
		return driver.findElement(add_to_cart_button);
	}
	public String carSize() throws InterruptedException {
		WebElement element = driver.findElement(test);
		if (!element.isDisplayed()) {
			System.out.println("Element not displayed");
		} 
		return element.getText();
		
	}
}
