package support;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;

public class DriverBase {
	public WebDriver driver;
	public Properties prop;
	
	public WebDriver initializerDriver() throws IOException {
		String fileLocation = "/Users/andersoncahet/Desktop/home-test/HomeTest/src/test/java/support/data.properties";
		FileInputStream fis = new FileInputStream(fileLocation);
		prop  = new Properties();
		prop.load(fis);
		String browserName = prop.getProperty("browser");
		
		if (browserName.equals("IE")) {
			driver = new InternetExplorerDriver();
			driver.manage().window().maximize();
			System.out.println("IE started");
			
		} else if (browserName.equals("Chrome")) {
			String driverLocation = prop.getProperty("chromeDriver");
			String driverKey = prop.getProperty("chromeDriverKey");
			System.setProperty(driverKey, driverLocation);
			driver = new ChromeDriver();
			driver.manage().window().fullscreen();
			System.out.println("Chrome started");
			
		} else if (browserName.equals("Safari")) {
			driver = new SafariDriver();
			driver.manage().window().maximize();
			System.out.println("Safari started");
			
		} else if (browserName.equals("Firefox")) {
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
			System.out.println("Firefox started");
		}
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver;
	}
}
